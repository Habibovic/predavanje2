package com.example.korisn.predavanje2;

import android.app.Activity;
import android.content.DialogInterface;
import android.renderscript.Sampler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;


public class MainActivity extends AppCompatActivity {
    EditText textInput;
    RelativeLayout scoreLayout;
    RelativeLayout gameLayout;
    TextView textScoreResult;
    TextView yourScore;

    int i; //inicijalizacija int
    int score;
    int bestScore;
    boolean firstTime = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textInput = (EditText) findViewById(R.id.edittext);
        scoreLayout = (RelativeLayout) findViewById(R.id.scoreLayout);
        gameLayout = (RelativeLayout) findViewById(R.id.gameLayout);
        textScoreResult = (TextView) findViewById(R.id.textScoreResult);
        yourScore = (TextView) findViewById(R.id.yourScore);


    }

    //metod za slucajni odabir brojeve izmedju 1 i 100
    public void random() {
        Random r = new Random();
        i = r.nextInt(100) + 1;
        Log.i("TAG", "Random number: " + i); // Za test
    }

    //na klik dugmica pokreni poredjenje
    public void clickMe(View view) {
        //Ako korisnik klikne prije unosa broja podesi na 0
        if (String.valueOf(textInput.getText()).equals("")) {
            textInput.setText("0");
        }
        //Prebaci uneseni string u int
        final int textInputResult = Integer.parseInt(String.valueOf(textInput.getText()));
        //setuj input na prazno
        textInput.setText("");
        //Povecaji pokušaje
        score++;

        //Provjeri da li je random broj pogodjen
        if (i == textInputResult) {


            //Pokreni skrivanje tastature ako je broj pogođen
            hideSoftKeyboard(this);
            //kreiraj alert dialog ako je pogodjen broj
            final AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Čestitamo!!!");
            alert.setMessage("Pogodili ste broj koji je tražen! \n Broj pokušaja: " + score);
            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    //Pokreni novi random
                    gameLayout.setVisibility(View.GONE);
                    scoreLayout.setVisibility(View.VISIBLE);

                    //Ako je novi rezutat mani od trenutnog novi rezultat je najbolji
                    if (score < bestScore || firstTime) {
                        bestScore = score;
                        firstTime = false;
                    }
                    //Ispiši najbolji - najmanji rezutat na scoreLayout
                    textScoreResult.setText(String.valueOf(bestScore));
                    //Ispiši moj rezultat na scoreLayout
                    yourScore.setText(String.valueOf(score));
                }
            });

            AlertDialog alertDialog = alert.create();
            alertDialog.show();
        }
        //ako uneseni broj nije isti kao random broj
        else {
            if (i > textInputResult) {
                Toast.makeText(this, "Broj koji tražite je veći!", Toast.LENGTH_SHORT).show();
                textInput.setText("");
            } else if (i < textInputResult) {
                Toast.makeText(this, "Broj koji tražite je manji!", Toast.LENGTH_SHORT).show();
                textInput.setText("");

            }
        }
    }

    public void clearInput(View view) {
        textInput.setText("");
    }

    public void newGame(View view) {
        random();
        gameLayout.setVisibility(View.VISIBLE);
        scoreLayout.setVisibility(View.GONE);
        //Resetuj brojač pokušaja
        score = 0;

    }

    //Skloni tastaturu nakon zavrsenog unosa - stackoverflow
    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }
}
